(function(root, factory) {
    if (typeof define === 'function' && define.amd) {
        // AMD. Register as an anonymous module.
        define([], factory);
    } else if (typeof module === 'object' && module.exports) {
        // Node. Does not work with strict CommonJS, but
        // only CommonJS-like environments that support module.exports,
        // like Node.
        module.exports = factory();
    } else {

        // Browser globals (root is window)
        root.ToggleButton = factory();
    }
}(this, function() {
    'use strict';

    var id = 0;

    function extend(receiver, supplier) {
        var property;

        for (property in supplier) {
            if (supplier.hasOwnProperty(property)) {
                receiver[property] = supplier[property];
            }
        }

        return receiver;
    }

    function ToggleButton(togglee, options) {

        var self = this,
            eventType;

        this.options = options = extend({
            document: document,
            window: window,
            idPrefix: 'toggler-',
            toggleeIdPrefix: 'togglee-',
            expandLabel: 'Expand',
            collapseLabel: 'Collapse',
            className: 'toggler',
            expand: false,
            insert: 'before'
        }, options);

        this.id = options.idPrefix + ++id;

        //togglee
        this.togglee = togglee;
        //Don't mess with existing ids
        togglee.id = togglee.id || options.toggleeIdPrefix + id;
        togglee.setAttribute('aria-labelledby', this.id);
        togglee.setAttribute('role', 'group');

        //toggler
        this.button = options.document.createElement('button');
        //html
        this.button.innerHTML = options.inactiveLabel;
        //id
        this.button.id = this.id;
        //class
        this.button.className = options.className;
        //attributes
        this.button.setAttribute('type', 'button');
        this.button.setAttribute('aria-owns', togglee.id);
        this.button.setAttribute('aria-controls', togglee.id);

        //click event for expand/collapse
        function expandCollapseListener() {
            self.button.getAttribute('aria-expanded') === 'true' ? self.collapse() : self.expand();
        }

        if (this.button.addEventListener) {
            this.button.addEventListener('click', expandCollapseListener, false);
        } else if (this.button.attachEvent)  {
            this.button.attachEvent('onclick', expandCollapseListener);
        }

        //custom events
        this._events = {};

        if (options.hasOwnProperty('on')) {
            for (eventType in options.on) {
                this.on(eventType, options.on[eventType]);
            }
        }

        //set default state
        options.expand ? this.expand() : this.collapse();

        //add button to DOM
        this.insert(options.insert);
    }
    ToggleButton.prototype.expand = function() {
        this.button.innerHTML = this.options.collapseLabel;
        this.button.setAttribute('aria-expanded', 'true');
        this.togglee.setAttribute('aria-expanded', 'true');
        this.togglee.setAttribute('aria-hidden', 'false');
        this.trigger('expand');
    };
    ToggleButton.prototype.collapse = function() {
        this.button.innerHTML = this.options.expandLabel;
        this.button.setAttribute('aria-expanded', 'false');
        this.togglee.setAttribute('aria-expanded', 'false');
        this.togglee.setAttribute('aria-hidden', 'true');
        this.trigger('collapse');
    };
    ToggleButton.prototype.insert = function(position) {
        if (position === 'before') {
            this.togglee.parentNode.insertBefore(this.button, this.togglee);
        } else {
            this.togglee.parentNode.insertBefore(this.button, this.togglee.nextSibling);
        }
    };
    ToggleButton.prototype.on = function(eventType, listener) {

        if (this._events.hasOwnProperty(eventType)) {
            this._events[eventType].push(listener);
        } else {
            this._events[eventType] = [listener];
        }
    };
    ToggleButton.prototype.trigger = function(eventType, params) {

        var i,
            events;

        //IE8 errors if undefined is passed to the function apply method.
        //Ensure it is an array.
        //https://msdn.microsoft.com/en-us/library/4zc42wh1%28v=vs.94%29.aspx
        params = arguments.length > 1 ? params : [];

        if (this._events.hasOwnProperty(eventType)) {

            events = this._events[eventType];

            for (i = 0; i < events.length; i++) {
                events[i].apply(this, params);
            }
        }
    };
    /*NavButton.prototype.off = function( eventType, callback ) {

        var i = 0,
            lenI,
            events;

        if ( eventType in this._events ) {

            //remove a specific callback for this event type
            if ( callback ) {

                events = this._events[eventType];
                lenI = events.length;

                for ( ; i < lenI; i++ ) {

                    if( events[i] === callback ) {

                        events.splice(i, 1);
                    }
                }
            } else {
                //remove all callbacks for the event type
                this._events[eventType].length = 0;
            }
        }
    };*/




    return ToggleButton;
}));









