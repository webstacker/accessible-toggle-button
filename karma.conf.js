module.exports = function(config) {
    config.set({

        // base path, that will be used to resolve files and exclude
        basePath: '',

        // frameworks to use
        frameworks: ['jasmine'],

        // list of files / patterns to load in the browser
        files: [
            //These scripts will be loaded in to the karma window
            //mocks

            //fixtures
            'tests/fixtures/*.html',

            //spec helpers
            'tests/helpers/*.js',

            //code
            'node_modules/phantomjs-polyfill/bind-polyfill.js',
            'node_modules/Sandbox/js/*.js',
            {pattern: 'js/*', included: false},

            //specs
            'tests/*.spec.js'
        ],

        // list of files to exclude
        exclude: [],

        // test results reporter to use
        reporters: ['mocha'],

        // web server port
        port: 9876,

        // enable / disable colors in the output (reporters and logs)
        colors: true,

        // level of logging
        logLevel: config.LOG_INFO,

        // enable / disable watching file and executing tests whenever any file changes
        autoWatch: true,

        // Start these browsers
        browsers: ['PhantomJS'],

        // If browser does not capture in given timeout [ms], kill it
        captureTimeout: 60000,

        // Continuous Integration mode
        // if true, it capture browsers, run tests and exit
        singleRun: false
    });
};
