var customMatchers = {
    toHaveAttributes: function(util, customEqualityTesters) {
        return {
            compare: function(actual, expected, message) {

                var actualAttributes = {},
                    result = {},
                    attribute,
                    failureMessageParams;

                if (expected === undefined) {
                    expected = {};
                }

                //for each expected attribute store the "actual" attribute and value
                for ( attribute in expected ) {

                    if (attribute === 'class') {
                        actualAttributes[attribute] = actual.className;
                    } else if ( attribute === 'id') {
                        actualAttributes[attribute] = actual.id;
                    } else {
                        actualAttributes[attribute] = actual.getAttribute(attribute);
                    }
                }

                result.pass = util.equals(actualAttributes, expected, customEqualityTesters);

                failureMessageParams = ['to have values', true, actualAttributes, expected, message];

                //If no "message" parameter is passed in, remove it otherwise 'undefined' is appended to failed spec messages.
                if (arguments.length === 2) {
                    failureMessageParams.pop();
                }

                if (result.pass) {

                    //negated error message
                    result.message = util.buildFailureMessage.apply(null, failureMessageParams);
                } else {

                    failureMessageParams[1] = false;

                    //standard error message
                    result.message = util.buildFailureMessage.apply(null, failureMessageParams);
                }

                return result;
            }
        };
    }
};


describe('accessible-toggle-button', function() {

    var cssId = 'links',
        fixture;

    beforeEach(function(done){

        jasmine.addMatchers(customMatchers);

        fixture = new Sandbox({
            url: '/base/tests/fixtures/navigation.html',
            scripts: ['/base/js/accessible-toggle-button.js'],
            complete: function() {
                done();
            }
        });
    });


    describe('Given a togglee element with no id', function(){
        it('should couple a button to the supplied togglee using aria attributes', function() {

            var togglee = fixture.document.querySelector('.togglee'),
                toggler = new fixture.window.ToggleButton(togglee);

            expect(toggler.button).toHaveAttributes({
                id: 'toggler-1',
                'aria-owns': 'togglee-1',
                'aria-controls': 'togglee-1'
            }, 'toggler');

            expect(togglee).toHaveAttributes({
                id: 'togglee-1',
                'aria-labelledby': 'toggler-1',
                role: 'group'
            }, 'togglee');
        });
    });



    describe('Given a togglee element with an id', function(){
        it('should couple a button to the supplied togglee using aria attributes and maintain the existing id', function() {

            var togglee = fixture.document.getElementById('togglee'),
                toggler = new fixture.window.ToggleButton(togglee);

            expect(toggler.button).toHaveAttributes({
                id: 'toggler-1',
                'aria-owns': 'togglee',
                'aria-controls': 'togglee'
            }, 'toggler');

            expect(togglee).toHaveAttributes({
                id: 'togglee',
                'aria-labelledby': 'toggler-1',
                role: 'group'
            }, 'togglee');
        });
    });

    it('should set the default expanded/collapsed state of the togglee element to collapsed', function() {

        var togglee = fixture.document.querySelector('.togglee'),
            toggler = new fixture.window.ToggleButton(togglee);

        expect(toggler.button).toHaveAttributes({
            'aria-expanded': 'false'
        }, 'toggler');

        expect(togglee).toHaveAttributes({
            'aria-expanded': 'false',
            'aria-hidden': 'true'
        }, 'togglee');
    });

    it('should allow the default expanded/collapsed state to be overridden', function() {
        var togglee = fixture.document.querySelector('.togglee'),
            toggler = new fixture.window.ToggleButton(togglee, {
                expand: true
            });

        expect(toggler.button).toHaveAttributes({
            'aria-expanded': 'true'
        }, 'toggler');

        expect(togglee).toHaveAttributes({
            'aria-expanded': 'true',
            'aria-hidden': 'false'
        }, 'togglee');
    });

    it('should by default insert the button before the togglee element', function() {
        var togglee = fixture.document.querySelector('.togglee'),
            toggler = new fixture.window.ToggleButton(togglee);

        expect(fixture.document.querySelector('#toggler-1 + .togglee')).not.toBeNull();
    });

    it('should allow the default insert position to be overridden', function() {
        var togglee = fixture.document.querySelector('.togglee'),
            toggler = new fixture.window.ToggleButton(togglee, {
                insert: 'after'
            });

        expect(fixture.document.querySelector('.togglee + #toggler-1')).not.toBeNull();
    });

    xit('should execute a callback when the button is clicked', function() {
        var togglee = fixture.document.querySelector('.togglee'),
            toggler = new fixture.window.ToggleButton(togglee),
            evt = fixture.document.createEvent('MouseEvents');

        evt.initEvent("click", true, true);
        toggler.button.dispatchEvent(evt);

        expect(toggler.button).toHaveAttributes({
            'aria-expanded': 'true'
        });
    });


    it('should allow custom expand events to be added', function(){
        var togglee = fixture.document.querySelector('.togglee'),
            toggler = new fixture.window.ToggleButton(togglee, {
                expand: true,
                on: {
                    expand: function() {
                        this.expanded = true;
                    }
                }
            });

        expect(toggler.expanded).toEqual(true);
    });

    it('should allow custom collapse events to be added', function(){
        var togglee = fixture.document.querySelector('.togglee'),
            toggler = new fixture.window.ToggleButton(togglee, {
                on: {
                    collapse: function() {
                        this.expanded = false;
                    }
                }
            });

        expect(toggler.expanded).toEqual(false);
    });
});
