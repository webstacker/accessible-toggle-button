Controls the active/inactive state of an accociated element. This is represented by ARIA roles, ARIA attributes, and CSS classes.
